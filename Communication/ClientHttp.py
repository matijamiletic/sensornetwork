from datetime import datetime
import requests
import json

class ClientHttp():
    #konstruktor prima url na koji ce klijentska strana slati podatke
    def __init__(self, url):
        self.url = url
    #slanje post upita na zadani url   
    def sendData(self, data): #TODO: funkcija bi trebla kao parametar primati http glagol koji se koristi prilikom slanja, ovdje koristimo samo post
        response = requests.post(self.url, json=json.dumps(data)) 
        #ispis responsa nakon poslanih podataka, npr {"success": "true"}
        print response.content
    