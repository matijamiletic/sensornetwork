from sensornetwork.Configuration.Configuration import Configuration
from datetime import datetime
import json
import time

class Client():

    @classmethod
    def sensorDataPacking(self, sensorId, data):
        
        deviceConfiguration = Configuration.loadConfiguration('dataRPI.json')
        for observation in data:
            observationName = observation

        for sensorElement in deviceConfiguration["sensors"]:
            if sensorElement["sensorId"] == sensorId:
                sensorType = sensorElement["sensorType"]
                sensorParameters = {"datastreamId": sensorElement["sensorParameters"]["datastreamId"][observationName]}
                break

        return {
            'type' :"sensor data",
            'data': data, 
            'deviceId': deviceConfiguration["deviceId"], 
            'sensorId': sensorId, 
            'sensorType': sensorType, 
            'timestamp': (datetime.utcnow() - datetime(1970, 1, 1)).total_seconds(), 
            'sensorParameters': sensorParameters
            }

    @classmethod
    def checkConfigurationPacking(self, deviceId):
        return{
            "type": "configuration check",
            "deviceId": deviceId
            }