from sensornetwork.Sensors.SensorSlm393 import SensorSlm393
from sensornetwork.Sensors.SensorDht22 import SensorDht22
from sensornetwork.Sensors.SensorRpi import SensorRpi
from sensornetwork.Sensors.SensorDht11 import SensorDht11
from sensornetwork.Sensors.SensorBiss0001 import SensorBiss0001
from sensornetwork.Sensors.SensorBluetooth import SensorBluetooth


class SensorBase(): 
    #mapiranje senzor klasa prema tipu senzora, kako bi kasnije mogli prema dobivenom tipu senzora stvarati instance senzora
    sensorConfiguration = {
            "dht22":{
                "sensorClass": SensorDht22
            },
            "slm393":{
                "sensorClass": SensorSlm393
            },
            "rpi":{
                "sensorClass": SensorRpi
            },
            "dht11":{
                "sensorClass": SensorDht11
            },
            "biss0001":{
                "sensorClass": SensorBiss0001
            },
            "bluetooth":{
                "sensorClass": SensorBluetooth
            }
    }
    
    #prema konziguraciji iz sensorConfiguration dohvaca se potreban senzor i radi instanca njegove klase
    @classmethod
    def getSensor(cls, sensorType, sensorParameters):
        sensorClassName = SensorBase.sensorConfiguration[sensorType]["sensorClass"]
        #svaka instanca senzora prima sensorParameters u konstruktor (npr pin na koji je prikljucen senzor)
        sensorObject = sensorClassName(sensorParameters)
        return sensorObject 
        
    #funkcija poziva funkciju za validaciju svakog senzora posebno (validacija se obavlja prema conditionu koji je poslan)
    @classmethod
    def validateCondition(cls, sensorData, condition, observationName):
        #dohvacanje trenutnog senzora i pozivanje funkcije za validaciju za taj odredeni senzor
        sensorObject = SensorBase.getSensor(sensorData["sensorType"], sensorData["sensorParameters"]);
        return sensorObject.validateCondition(sensorData, condition, observationName)




