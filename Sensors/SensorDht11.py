#import Adafruit_DHT #na rpiu odkomentirati
import json
from sensornetwork.Configuration.Configuration import Configuration

class SensorDht11():

    #svaki senzor prima prilikom inicijalizacija sensorParameters, ta varijabla sadrzi podatke o parametrima senzora, npr: pinovi na koje je prikljucen
    def __init__(self, sensorParameters):
        self.sensorParameters = sensorParameters;

    #dohvacanje podataka za senzor, svakki senzor ima drugacije podatke
    def getData(self):
        #humidity, temperature = Adafruit_DHT.read_retry(11,4)
        data={"temperature": 1, "humidity": 25}

        #napraviti da odvojeno vraca temepraturu a odvojeno vlagu
        #data = {"temperature": 12.54}
        return data

    #svaki senzor ima za sebe validaciju podataka koje je poslao i zadane uvijete prema kojima se validacija obavlja
    @classmethod
    def validateCondition(cls, data, condition, observationName):

        if(condition["dataCondition"] == "none"):
            return False


        condition1 = condition["dataCondition"][observationName][0]
        condition2 = condition["dataCondition"][observationName][1]
        
        #if(str(condition1)=="none" and str(condition2) == "none"):
         #   return False


        if (data["data"][observationName]>condition1 and data["data"][observationName]<condition2):
            notifications = Configuration.loadConfiguration('androidNotifications.json')
            datastreams = Configuration.loadConfiguration('datastreams.json')
            datastream = data["sensorParameters"]["datastreamId"]
            for element in datastreams:
                if element["datastreamId"]==datastream:
                    datastreamIot = element["iot.id"]
            notifications["condition"] = "True"
            notifications["notifications"].append({
                "datastreamId": datastreamIot,
                "data": data["data"][observationName] 
                })
            Configuration.saveConfiguration(notifications, 'androidNotifications')
            return True
        else:
            return False