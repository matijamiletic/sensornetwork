import json

class Configuration():

	@classmethod
	def loadConfiguration(cls, fileName):
		try:
			with open(fileName) as dataFile:
				try:
					dataFile = json.load(dataFile)
				except ValueError:
					print "Can't load file %s\n" % fileName
					return
				#print dataFile
		except IOError:
			print "Cant open file %s" % (fileName)
			return

		return dataFile

	@classmethod
	def saveConfiguration(cls, data, outputFileName):
		with open(outputFileName + '.json', 'w') as outputFile:
			try:
				json.dump(data, outputFile, sort_keys=True, indent=4)
			except ValueError:
				print "Cant write data to file, check if JSON"