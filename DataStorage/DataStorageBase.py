import MySQLdb
import json
import sys

class DataStorageBase():
    #konstruktor prima parametre potrebne za spajanje na bazu
    #storageType oznacava mogucnost stvaranja razlicitih konekcija
    def __init__(self, storageType, storageParameters):
        try:
            #spajanje na mysql bazu podataka, potrebni parametri: hostname, username, pass i ime baze (db)
            if(storageType == 'mysql'):
                self.dataBaseConnection=MySQLdb.connect(
                    host = storageParameters['hostname'],
                    user = storageParameters['user'],
                    passwd = storageParameters['password'],
                    db = storageParameters['dbName']
                )
        #ispis pogreske ako povezivanje na bazu nije uspjelo
        except Exception as error:
            sys.exit('Unable to connect to db!')

        if(self.dataBaseConnection):
            print "Database connection enabled"
    #citanje podataka zapisanih u bazu podataka     
    def read(self, conditions):
        cursor = self.dataBaseConnection.cursor()
        result = list()

        self.query =  self.createQuery(conditions)

        print self.query #printati dok testiramo

        cursor.execute(self.query)

        fetchedRow = self.fetchOneAssoc(cursor)
        while fetchedRow is not None:
            result.append(fetchedRow)
            fetchedRow = self.fetchOneAssoc(cursor)

        self.dataBaseConnection.commit() #commitamo promjene s lokalnog na bazu podataka
        return result

    #dohvacanje jedog retka iz tablice
    def fetchOneAssoc(self, cursor):
        fetchedData = cursor.fetchone()
        if fetchedData == None:
            return None
        dataDescription = cursor.description

        result = {}

        for (name, value) in zip(dataDescription, fetchedData):
            result[name[0]] = value

        return result
    #svaki upit u bazu je razlicit pa ovisno o zadanom upitu kreira se mysql naredba
    #ovaj dio koristi se na frontendu za dohvacanje filtriranih podataka
    def createQuery(self, conditions):
        tableName = 'sensordata'
        sqlQuery = list()
        sqlQuery.append("SELECT * FROM %s " % tableName)
        limit = ''
        offset = ''
        timeFrom = 0.0
        timeTo = 0.0

        if 'limit' in conditions:
            limit = conditions['limit']
            del conditions['limit']

        if 'offset' in conditions:
            offset = conditions['offset']
            del conditions['offset']

        if 'timeFrom' in conditions:
            timeFrom = float(conditions['timeFrom'])
            del conditions['timeFrom']

        if 'timeTo' in conditions:
            timeTo = float(conditions['timeTo'])
            del conditions['timeTo']

        if conditions:
            sqlQuery.append("WHERE " + " AND ".join("%s = '%s'" % (key, value) for key, value in conditions.iteritems()))

        if not(conditions) and (timeFrom!=0.0 or timeTo!=0.0):
            sqlQuery.append("WHERE ")
        elif timeFrom!=0.0 or timeTo!=0.0:
            sqlQuery.append("AND ")

        if timeFrom!=0.0 and timeTo!=0.0:
            sqlQuery.append("timestamp BETWEEN %f AND %f" % (timeFrom, timeTo))
        elif timeFrom!=0.0:
            sqlQuery.append("timestamp>=%f" % timeFrom)
        elif timeTo!=0.0:
            sqlQuery.append("timestamp<=%f" % timeTo)

        if limit:
            sqlQuery.append(" LIMIT %s" % limit)

        if offset:
            sqlQuery.append(" OFFSET %s" % offset)

        query =  "".join(sqlQuery)
        return query
    #zapisivanje podataka o senzoru bazu podataka   
    def add(self, data):
        cursor = self.dataBaseConnection.cursor()
        cursor.execute("INSERT INTO sensordata (deviceID, sensorID, sensorType, timestamp, data) VALUES (%s, %s, %s, %s, %s)", (data['deviceId'], data['sensorId'], data['sensorType'], data['timestamp'], json.dumps(data['data'])))
        self.dataBaseConnection.commit() #commitamo promjene s lokalnog na bazu podataka        
            
    #zatvaranje konekcije na bazu    
    def close(self):
        #db.commit() #commitamo promjene s lokalnog na bazu podataka
        self.dataBaseConnection.close()