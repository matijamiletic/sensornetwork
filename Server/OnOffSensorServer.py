from sensornetwork.Configuration.Configuration import Configuration
import json 
class OnOffSensorServer():

    def handleData(self, inputData):
        print "ON OFF"
        print inputData
        data = Configuration.loadConfiguration('data.json')
        datastreams = Configuration.loadConfiguration('datastreams.json')
        datastreamIot = json.loads(inputData)["datastreamId"]
        
        for element in datastreams:
            if element["iot.id"] == int(datastreamIot):
                datastreamId = element["datastreamId"]


        for device in data:
            for sensor in device["sensors"]:
                for datastream in sensor["sensorParameters"]["datastreamId"]:
                    if sensor["sensorParameters"]["datastreamId"][datastream] == datastreamId:
                        device["changeConfiguration"] = "True"
                        sensor["sensorParameters"]["sensorOn"] = json.loads(inputData)["sensorOn"]

        Configuration.saveConfiguration(data, 'data')

        response = json.dumps({'success': 'True'})

        return response     



