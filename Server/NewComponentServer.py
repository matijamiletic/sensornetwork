from sensornetwork.Configuration.Configuration import Configuration
import json 
class NewComponentServer():

    def handleData(self, component):
      data = Configuration.loadConfiguration("data.json")

      conditions = Configuration.loadConfiguration("notificationConditions.json")
      component = json.loads(component)
      

      if("sensorId" in component and "thingId" in component and "sensorType" in component and "datastreams" in component):
          if ("sensorId" in component): #ako se dodaje novi senzor
            datastreamsMapping = Configuration.loadConfiguration("datastreams.json")
        
            numberOfDatastreams = len(datastreamsMapping)

            newCondition = {
            "dataCondition": {
            }, 
            "deviceId": int(component["thingId"]), 
            "sensorId": int(component["sensorId"]), 
            "sensorType": component["sensorType"].lower()
          }

            datastreamIds = {}

            for datastream in component["datastreams"]:
              newCondition["dataCondition"][datastream] = "none"
              print numberOfDatastreams
              newDatastream = {"datastreamId": numberOfDatastreams+1, "iot.id": int(component["datastreams"][datastream])}
              numberOfDatastreams+=1
              print numberOfDatastreams
              datastreamIds[datastream] = numberOfDatastreams
              datastreamsMapping.append(newDatastream)

            conditions.append(newCondition)
            Configuration.saveConfiguration(conditions, "notificationConditions")
            Configuration.saveConfiguration(datastreamsMapping, "datastreams")

     
        

        #for datastrm in component["datastreams"]:
        #  datastreamIds[datastrm] = int(component["datastreams"][datastrm])

            for device in data:
              if (device["deviceId"] == int(component["thingId"])):
                device["changeConfiguration"] = "True"
                newSensor = {
                    "sensorId": int(component["sensorId"]), 
                    "sensorParameters": {
                        "datastreamId": datastreamIds, 
                        "sensorOn": "True"
                    }, 
                    "sensorType": component["sensorType"].lower()
                    }    
                device["sensors"].append(newSensor)
            Configuration.saveConfiguration(data, "data")


            print "Sensor added"
            response = json.dumps({"success": "True"})

      elif ("deviceId" in component): #ako se dodaje novi device
        newComponent = {
            "changeConfiguration": "False", 
            "deviceId": int(component["deviceId"]), 
            "sensors": []
        }

        data.append(newComponent)
        Configuration.saveConfiguration(data, "data")
        response = json.dumps({"success": "True"})
        print "Device added"
      
      else:
          response = json.dumps({"success": "False"})  
      return response




