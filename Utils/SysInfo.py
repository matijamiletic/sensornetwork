import os #omogucuje koristenje funkcionalnosti os-a, citanje, pisanje, manipulacija path-ovima
import re #omogucuje koristenje regularnih izraza

class SysInfo(object):

    def __init__(self):
        self.temperature = re.compile("temp=(.+)'C") #kompajliranje regularnog izraza u regularni objekt kako bi ga mogli koristiti u search i match funkcijama
        self.volts = re.compile("volt=(.+)V")
        self.ram = re.compile("MemFree:(.+)kB")

    def getSysInfo(self):
        return {
            "CpuTemperature": self.getCpuTemperature(),
            "PhysicalRam": self.getPhysicalRam(), #ako id nije zadan po defaltu je core
        }


    def getCpuTemperature(self):
        return self.getVcgenCmdInfo("measure_temp", self.temperature)

    def getVcgenCmdInfo(self, parameters, regex):
        result = os.popen("vcgencmd " + parameters).readline() #popen - otvara pipe, return value je file koji se moze citati ili se moze pisati po njemu
        matchResult = regex.match(result) #regularni izrazi izvuc samo br iz dobivenog data jer vraca temp = '
        return matchResult.group(1) #uzmi prvu podgrupu, samo vrijednost temperature

    def getPhysicalRam(self):
        result = str(os.popen("awk '/MemFree/ { print $2 }' /proc/meminfo").readline())
        result = result[:-1]
        return result
    


